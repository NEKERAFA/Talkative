![Talkative](public/icon.png)

[![Powered by Lua](https://img.shields.io/badge/lua-5.3-blue.svg)](https://www.lua.org/) [![Made by Löve](https://img.shields.io/badge/love2d-11.0-e64998.svg)](https://love2d.org/)

A dialogues manager for LÖVE.

![Example of use](screenshot.png)

## Examples

There is an example of use in the root folder.


```lua
talkative = require "talkative"

function love.load()
	dialogueSystem = talkative:new()
	dialogueSystem:addLine({"Hello world"})
end

function love.update(dt)
	dialogueSystem:update(dt)
end

function love.draw()
	dialogueSystem:draw(0, 0)
end
```

## Documentation

You can also see the documentation in https://nekerafa.gitlab.io/Talkative/

## License

> MIT License
>
> Copyright (c) 2017-2018 Rafael Alcalde Azpiazu
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
