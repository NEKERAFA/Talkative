-- A simple markup language parser for riched text.
--
-- @author Rafael Alcalde Azpiazu
-- @license MIT License

local xml = {}

-- Find a tag element in the string and return the name and the open and close
-- symbols if exists.
-- @param str A string with contains tags
-- @param i The first position to search
-- @param j The second position to search
function xml.get_tag(str, i, j)
  if not i then
    return string.find(str, "<(%/?)(%w+)%s*(%/?)>")
  end

  if not j then
    return string.find(str, "<(%/?)(%w+)%s*(%/?)>", i)
  end

  return string.find(str, "<(%/?)(%w+)%s*(%/?)>", i, j)
end

-- Parse a string with marks and converts into a lua table.
-- @param str The string to parse.
-- @return A table with all substring and marks sorts in order of appearance.
-- The marks contain the same structure, but they have an additional key, name,
-- which is the name of the mark.
function xml.parse(str)
  assert(type(str) == "string", "bad argument #1 to check (string expected, got "..type(str)..")")

  local root = {name = "root"}
  local top = root
  local tags = {{name = "root", node = root}}
  local pos = 1
  local finish = false

  while not finish do
    local first, last, open, tag, close = xml.get_tag(str, pos)

    if first then
      local text = string.sub(str, pos, first-1)
      text = string.gsub(text, "%&quot;", "\"")
      text = string.gsub(text, "%&amp;", "&")
      text = string.gsub(text, "%&apos;", "\'")
      text = string.gsub(text, "%&lt;", "<")
      text = string.gsub(text, "%&gt;", ">")
      table.insert(top, text)

      if open == "" and close == "" then -- New tag <tag>
        local new_top = {name = tag}
        table.insert(top, new_top)
        table.insert(tags, {name = tag, node = new_top})
        top = new_top
      elseif close == "" then -- New tag </tag>
        if #tags < 2 then
          error("parse error (tag <" .. tag .. "> not open)")
        end

        local open_tag = tags[#tags]

        if open_tag.name ~= tag then
          error("parse error (attempt to close " .. open_tag.name .. " with " .. tag .. ")")
        end

        table.remove(tags)

        local last_tag = tags[#tags]
        top = last_tag.node
      else -- Tag with close <tag/>
        table.insert(top, {name = tag})
      end

      pos = last+1
    else
      finish = true
    end
  end

  if #tags > 1 then
    error("parse error (unclosed tag <" .. tags[#tags].name .. ">)")
  end

  local text = string.sub(str, pos)
  text = string.gsub(text, "%&quot;", "\"")
  text = string.gsub(text, "%&amp;", "&")
  text = string.gsub(text, "%&apos;", "\'")
  text = string.gsub(text, "%&lt;", "<")
  text = string.gsub(text, "%&gt;", ">")
  table.insert(top, text)

  return root
end

return xml
