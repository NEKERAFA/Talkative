-- Example of a dialogue in Talkative system

local talkative = require "talkative"

local alpha = 0
local finish = false
local added = false

function love.load()
	-- Loads all the images
	bg1 = love.graphics.newImage("background_1.png")
	bg2 = love.graphics.newImage("background_2.png")
	neutral = love.graphics.newImage("Neutral.png")
	neutral_face = love.graphics.newImage("Neutral-Face.png")
	happy = love.graphics.newImage("Happy.png")
	person = neutral

	-- Loads the library
	dialogueSystem = talkative.new({interline = 0, height = 71})
	--dialogueSystem._debug = true -- This is for debugging purpose
	dialogueSystem:setBackground({0.25, 0.5, 1, 0.5})

	-- Puts all the script lines in the dialogue system
	dialogueSystem:addLine({
		"<character>Maid</character>\n" ..
		"Hi visitor!\n" ..
		"This is a demo of Talkative library using a example character.",

		function()
			person = happy
		end
	})
	dialogueSystem:addLine({
		"I&apos;m a maid example character and I hope you enjoy if you use it in " ..
		"RPG or in graphic novels ^~^",

		function()
			dialogueSystem:setPadding({5,50,5,5})
			finish = true
		end
	})
end

function love.keypressed(key)
	-- For skipping the text
	if key == "return" then
		dialogueSystem:skip()
	end
end

function love.update(dt)
	if finish and alpha < 1 then
		alpha = math.min(alpha + dt, 1)
	end

	if not finish or finish and alpha == 1 then
		-- Update the dialogue system
		dialogueSystem:update(dt)

		if finish and not added then
			added = true
			dialogueSystem:addLine({
				"Now I'm using maid like icon in this example text.",

				function()
					love.event.quit(0)
				end
			})
		end
	end
end

function love.draw()
	love.graphics.setColor(1, 1, 1, 1-alpha)
	love.graphics.draw(bg1, 320, 240, 0, 1, 1, bg1:getWidth()/2, bg1:getHeight()/2)
	love.graphics.setColor(1, 1, 1, alpha)
	love.graphics.draw(bg2, 320, 240, 0, 1, 1, bg2:getWidth()/2, bg2:getHeight()/2)

	love.graphics.setColor(1, 1, 1, 1)
	if not finish then
		love.graphics.draw(person, 0, 80)
	end

	-- Draws the dialogue box
	dialogueSystem:draw(0, 409)

	if finish and alpha == 1 and not dialogueSystem:isEmpty() then
		love.graphics.draw(neutral_face, 10, 424.5)
	end
end
