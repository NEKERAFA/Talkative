--- <h1 style="text-align:center"><img src="icon.png" alt="Talkative"/></h1>
-- [![Powered by Lua](https://img.shields.io/badge/lua-5.3-blue.svg)](https://www.lua.org/) [![Made by Löve](https://img.shields.io/badge/love2d-11.0-e64998.svg)](https://love2d.org/)
--
-- A dialogues manager for LÖVE.
--
-- <h2>Examples</h2>
-- There is an example of use in the root folder.
--
-- 	talkative = require "talkative"
--
-- 	function love.load()
-- 		dialogueSystem = talkative:new()
-- 		dialogueSystem:addLine({"Hello world"})
-- 	end
--
-- 	function love.update(dt)
-- 		dialogueSystem:update(dt)
-- 	end
--
-- 	function love.draw()
-- 		dialogueSystem:draw(0, 0)
-- 	end
--
-- <h2>Source code</h2>
-- <p>You can download Talkative and view the code on gitlab repository: <a href="https://gitlab.com/NEKERAFA/Talkative">https://gitlab.com/NEKERAFA/Talkative</a>.</p>
-- <p>You may also download the source code as a
-- <a href="https://gitlab.com/NEKERAFA/Talkative/uploads/39b4ad261c6ac1f8d895a76a70d5b731/talkative-2.0-src.tar.gz">tar.gz</a> or
-- <a href="https://gitlab.com/NEKERAFA/Talkative/uploads/64f694418145635d682ec13eadfe602e/talkative-2.0-src.zip">zip</a> file.
--
-- @module talkative
-- @author Rafael Alcalde Azpiazu
-- @release 1.0
-- @license MIT License


local talkative = {}
talkative.__index = talkative
talkative._VERSION = "1.0"
talkative._AUTHOR = "Rafael Alcalde Azpiazu"
talkative._DESCRIPTION = "A simple dialogue system for love2d"
talkative._URL = "https://github.com/NEKERAFA/rpg-talk"
talkative._LICENSE = [[MIT License

Copyright (c) 2017 Rafael Alcalde Azpiazu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.]]

local args = ...

-- Carga un módulo que está en la carpeta actual de talkative
function load_module(name)
	local current_module = args:gsub("%.init$", "")
	return require (current_module .. "." .. name)
end

-- Cargo el resto de módulos que se necesitan
local utf8  = require "utf8"

local tween = load_module "tween"
local xml   = load_module "xml"

-- Comprueba que un argumento se corresponda con el tipo que se pide.
local function check_argument(func, arg, type_arg, pos, default)
	local check = arg and ((type(arg) == type_arg) or
	                (arg.type and arg:type() == type_arg))

	-- Si no tiene el tipo adecuado devuelve el valor por defecto o lanza una
	-- excepción
	if not check then
		if default ~= nil then
			return default
		else
			error("bad argument #" .. pos .. " to " .. func .. " (" .. type_arg ..
			        " expected, got " .. type(arg) .. ")")
		end
	else
		return arg
	end
end

-- Función para obtener substring usando la biblioteca de utf8
local function str_sub(str, i, j)
	local offset_i = utf8.offset(str, i)
	local offset_j = utf8.offset(str, j or -1)

	return string.sub(str, offset_i, offset_j)
end

-- Crea un nuevo item de texto
local function create_item(text, style)
	local texture = love.graphics.newText(style.font)
	local width = style.font:getWidth(text)

	return {text = text, style = style, width = width, texture = texture}
end

-- Crea un item y lo añade al resto de textos
local function add_text(text, tags, lines, current_width, space)
	local item = create_item(text, tags[tags.size].style)
	table.insert(lines[lines.size], item)

	-- Si hay un texto más grande, se amplia el espacio a imprimir
	if lines[lines.size].height < item.style.font:getHeight() then
		lines[lines.size].baseline = item.style.font:getBaseline()
		lines[lines.size].height = item.style.font:getHeight()
	end

	-- Si hay un espacio después, indica que el item tiene después ese espacio
	if space then
		item.space = true
		return current_width + item.width + item.style._space
	end

	return current_width + item.width
end

-- Añade un texto y crea una nueva línea
local function new_line(text, tags, lines)
	add_text(text, tags, lines, 0)
	table.insert(lines, {baseline = 0, height = 0})
	lines.size = lines.size + 1
end

-- Dado un texto, lo parsea, obtiene los términos y crea una lista las lineas de
-- texto a imprimir
local function push_line(manager, str)
	-- Lineas de texto. Cada linea de texto contiene la linea base sobre la que se
	-- asienta el texto a la hora de imprimir y el alto de la linea
	local lines = {{baseline = 0, height = 0}, size = 1}
	-- Tabla que contiene el texto parseado
	local tree = xml.parse(str)
	-- Estilos guardados (para volver entre los distintos nodos)
	local tags = {{style = manager.style.default, node = tree, pos = 1}, size = 1}
	local current = tree -- Nodo actual
	local pos = 1 -- Hijo actual en el nodo
	local finished = false -- Si ha terminado el juego
	local margins = manager.style.default.margin[2] + manager.style.default.margin[4]
	local paddings = manager.style.default.padding[2] + manager.style.default.padding[4]
	local max_width = manager.width - margins - paddings -- Tamaño máximo del texto
	local current_width = 0 -- Ancho actual
	local term = ""

	-- Se recorre los nodos parseados para obtener el texto a imprimir
	while not finished do
		if pos <= #current then
			node = current[pos] -- Se obtiene un hijo

			if type(node) == "table" then -- Si el hijo es un nodo nuevo, se añade como un estilo nuevo y se explora
				assert(manager.style[node.name], "attempt to add text from a undefined style <" .. node.name .. ">")

				if #node > 0 then -- Si es un nodo se guarda su estilo
					table.insert(tags, {style = manager.style[node.name], node = node, pos = pos})
					tags.size = tags.size + 1
					current = node
					pos = 1
				end
			else -- Si el hijo es un string, se divide en términos mediante caracteres de espacio y se guardan en la lista final
				if node ~= "" then
					for char in string.gmatch(node, "([%z\1-\127\194-\244][\128-\191]*)") do
						-- Controlamos un salto de línea
						if char == "\n" then
							new_line(term, tags, lines)
							term = ""
							current_width = 0
						-- Controlamos un espacio
						elseif char == " " then
							current_width = add_text(term, tags, lines, current_width, true)
							term = ""
						-- Añadimos un nuevo caracter
						else
							-- Se comprueba que no supere el ancho
							local style_font = tags[tags.size].style.font
							if current_width + style_font:getWidth(term .. char) > max_width then
								-- Si es muy grande el texto, se warpea
								if current_width == 0 then
									new_line(term .. char, tags, lines)
									term = ""
									current_width = 0
								-- Si no, se pasa a la línea siguiente
								else
									table.insert(lines, {baseline = 0, height = 0})
									lines.size = lines.size + 1
									term = term .. char
									current_width = 0
								end
							else
								term = term .. char
							end
						end
					end

					if term ~= "" then -- Comprobamos que no quede ningún texto
						current_width = add_text(term, tags, lines, current_width)
						term = ""
					end
				end
				pos = pos+1
			end
		else
			-- Si aún no se ha vuelto al nodo raíz, se pasa al siguiente
			if tags.size > 1 then
				local tag = table.remove(tags)
				tags.size = tags.size - 1
				local top = tags[tags.size]
				pos = tag.pos+1
				current = top.node
			else
				finished = true
			end
		end
	end

	-- Se inserta las lineas cargadas en el manager
	table.insert(manager.script, lines)
	manager.script.lines = manager.script.lines+1
end

-- Comprueba si quedan lineas de diálogo restantes y prepara una línea
local function pop_line(manager)
	if manager.script.lines > 0 then
		manager.dialogue = {dt = 0, line = 1, word = 1, char = 0, lines = table.remove(manager.script, 1)}
		manager.script.lines = manager.script.lines - 1
		manager.top = 0
		manager.current_height = manager.dialogue.lines[1].height
	else
		manager.dialogue = nil
	end
end

--- Creates a new system manager.
-- @param args A table that contains:
-- <ul>
-- <li><code>cps</code>: Characters per second showed. 4 by default.</li>
-- <li><code>autoskip</code>: Sets auto skip. false by default.</li>
-- <li><code>delay</code>: Delay of auto skip dialogue in seconds. 2 by default.</li>
-- <li><code>format</code>: Format of charater image. "background" (default) for a complete
-- character image or "icon" for small character image.</li>
-- <li><code>transition</code>: Transition between character image. "fade" (default),
-- "replace" or "fade-replace".</li>
-- <li><code>mode</code>: Mode of appearance of charater image. "once" (default) or
-- "acummulative".</li>
-- </ul>
function talkative.new(args)
	if not args then
		args = {}
	end

	local obj = setmetatable({}, talkative)

	-- Lista de diálogos
	obj.script = {
		lines = 0
	}

	-- Lista de los estilos por defecto
	obj.style = {
		default = {
			font = love.graphics.newFont(14),
			color = {1, 1, 1},
			background = {0.1, 0.3, 1, 0.75},
			margin = {5, 5, 5, 5},
			padding = {5, 5, 5, 5}
		},

		character = {
			font = love.graphics.newFont(16),
			color = {0.75, 0.75, 0.75},
			background = {0, 0, 0, 0},
		}
	}
	-- Meta-información de los estilos
	obj.style.default._space = obj.style.default.font:getWidth(" ")
	obj.style.character._space = obj.style.character.font:getWidth(" ")

	-- Variables del programa
	obj.cps = check_argument("new", args[1] or args.cps, "number", 1, 16)
	obj.autoskip = check_argument("new", args[2] or args.autoskip, "boolean", 2, false)
	obj.delay = check_argument("new", args[3] or args.delay, "number", 3, 1)
	obj.interline = check_argument("new", args[4] or args.interline, "number", 4, 7)
	obj.width = check_argument("new", args[5] or args.width, "number", 5, ({love.window.getMode()})[1])
	obj.height = check_argument("new", args[6] or args.height, "number", 6, 60)
	obj.waiting = false

	return obj
end

--- Sets characters per second.
-- @param cps Characters per second showed.
function talkative:setCps(cps)
	check_argument("setCps", cps, "number", 1)
	self.cps = cps
end

--- Sets the delay of auto skip.
-- @param delay Delay of auto skip dialogue in seconds.
function talkative:setDelay(delay)
	check_argument("setDelay", delay, "number", 1)
	self.delay = delay
end

--- Sets the auto skip.
-- @param autoskip true for auto skip and false otherwise.
function talkative:setAutoSkip(autoskip)
	check_argument("setAutoSkip", autoskip, "boolean", 1)
	self.autoskip = autoskip
end

--- Sets the width of colored background.
-- @param width Width of background.
function talkative:setWidth(width)
	check_argument("setWidth", width, "number", 1)
	self.width = width
end

--- Sets the height of colored background.
-- @param height Heigth of background.
function talkative:setHeight(height)
	check_argument("setHeight", height, "number", 1)
	self.height = height
end

--- Sets the interline of text in dialogue box.
-- @param interline Interline of dialogue box in pixel.
function talkative:setInterline(interline)
	check_argument("setInterline", interline, "number", 1)
	self.interline = interline
end

--- Sets a background color.
-- @param bg A table with rgba color
function talkative:setBackground(bg)
	check_argument("setBackground", bg, "table", 1)

	-- Elimina una imagen de fondo
	if self.style.default.background_image then
		self.style.default.background_image:release()
		self.style.default.background_image = nil
		self.width = ({love.window.getMode()})[1]
		self.height = 68
	end

	self.style.default.background = bg
end

--- Sets a background image.
-- @param img A Image object
function talkative:setBackgroundImage(img)
	check_argument("setBackgroundImage", img, "Image", 1)

	self.style.default.background_image = img
	self.width = img:getWidth()
	self.height = img:getHeight()
end

--- Sets the padding of the text box.
-- @param padding A table that contains the top-left-bottom-right padding
function talkative:setPadding(padding)
	check_argument("setPadding", padding, "table", 1)

	self.style.default.padding = padding
end

--- Sets the margins of the text box.
-- @param margin A table that contains the top-left-bottom-right margin
function talkative:setMargin(margin)
	check_argument("setMargin", margin, "table", 1)

	self.style.default.margin = margin
end

--- Sets a new style of a text mark or overrides if the text mark style exists.
-- @param name The name of the style
-- @param style A table that contains the following keys:
-- <ul>
-- <li><code>font</code>: A Font object that represents the font with the text that will be displayed.</li>
-- <li><code>color</code>: The color of the text.</li>
-- <li><code>background</code>: The background color of the boc that contains the text.</li>
-- </ul>
function talkative:setStyle(name, style)
	if not self.style[name] then
		self.style[name] = {}
	end

	local style_tbl = self.style[name]
	local style_dft = self.style.default

	-- Pone por defecto el estilo que ya estiviera creado o el estilo por defecto
	style_tbl.font =
		check_argument("setStyle", style.font, "Font", "font", style_tbl.font or style_dft.font)
	style_tbl.color =
		check_argument("setStyle", style.color, "table", "color", style_tbl.color or style_dft.color)
	style_tbl.background =
		check_argument("setStyle", style.background, "table", "background", style_tbl.background or {0,0,0,0})
	style_tbl._space = style_tbl.font:getWidth(" ")
end

--- Adds a dialogue line into the dialogue system.
-- @param args A table that contains the following keys:
-- <ul>
-- <li><code>text</code>: The new line that will be added.</li>
-- <li><code>onfinish</code>: A callback function that will be called when the text finish printing on screen.</li>
-- </ul>
function talkative:addLine(args)
	local text = check_argument("addLine", args[1] or args.text, "string", 1)
	push_line(self, text)

	if args[2] or args.onfinish then
		local onfinish = check_argument("addLine", args[2] or args.onfinish, "function", 2)
		self.script[self.script.lines].onfinish = onfinish
	end

	if self.dialogue == nil then
		pop_line(self)
	end
end

--- Checks if the system is empty.
function talkative:isEmpty()
	return self.script.lines == 0 and not self.dialogue
end

--- Clear all dialogue saved in the system.
function talkative:clear()
	self.dialogue = nil
	self.script = {lines = 0}
	self.characters = {left = {}, right = {}}
end

--- This function prints all the remaining text or skip to the next line otherwise.
function talkative:skip()
	if self.dialogue then
		local info = self.dialogue
		local style_dft = self.style.default
		local margins = style_dft.margin[1] + style_dft.margin[3]
		local paddings = style_dft.padding[1] + style_dft.padding[3]
		local max_height = self.height - margins - paddings

		-- Comprobamos si no se ha terminado de imprimir el texto en pantalla
		if not self.waiting then
			local line = info.line
			local word = info.word
			-- Pasamos por todas las líneas y por todas las palabras seteando el texto
			while line <= info.lines.size do
				while word <= #info.lines[line] do
					info.lines[line][word].texture:set(info.lines[line][word].text)
					word = word + 1
				end

				--  Por si tenemos que levantar el texto
				if line < info.lines.size then
					local new_height = self.current_height + self.interline + info.lines[line+1].height
					self.current_height = new_height

					-- Si hay que levantar el texto, esperamos
					if self.current_height - self.top > max_height then
						self.waiting = true
						break
					else
						word = 1
					end
				end

				line = line + 1
			end

			if line > info.lines.size then
				line = info.lines.size
			end

			-- Seteamos los valores al final
			info.line = line
			info.word = #info.lines[info.line]
			info.char = utf8.len(info.lines[info.line][info.word].text)
		-- Si hemos llegado al final de una linea y queda texto, levantamos las lineas para mostrar más texto
		elseif info.line < info.lines.size then
			self.waiting = false
			info.word = 1
			self.current_height = new_height
			info.line = info.line+1
			info.char = 0
			info.dt = 0

			self.line_tween = tween.new(0.5, self, {top = self.top - (info.lines[info.line-1].height + self.interline)}, "inOutQuad")
		-- Si hemos llegado al final, ponemos otra linea
		else
			self.waiting = false
			if info.lines.onfinish then
				info.lines.onfinish()
			end
			pop_line(self)
		end
	end
end

--- Update the internal status of the manager.
-- @tparam number dt Time since the last update in seconds.
function talkative:update(dt)
	-- Comprueba si existe un diálogo
	if self.dialogue then
		-- Se comprueba si hay una animación
		if self.line_tween then
			local complete = self.line_tween:update(dt)
			if complete then
				self.line_tween = nil
			end
		end

		local info = self.dialogue
		info.dt = info.dt + dt -- Se actualiza el delta
		-- Comprueba que no se haya llegado al final del diálogo
		if info.line < info.lines.size or info.word < #info.lines[info.line] or info.char < utf8.len(info.lines[info.line][info.word].text) then
			local chars = self.cps * info.dt -- Se obtiene el número de caracteres

			-- Si hay uno o más caracteres que sacar, se procesan
			if chars >= 1 then
				-- Si no se ha completado una palabra, se añaden los caracteres a las texturas
				if info.char < utf8.len(info.lines[info.line][info.word].text) then
					info.char = info.char + 1
					local item = info.lines[info.line][info.word]
					item.texture:set(str_sub(item.text, 1, info.char))
					info.dt = info.dt - 1 / self.cps
				-- Si se ha completado, se añade el espacio y se pasa al siguiente término
				elseif info.word < #info.lines[info.line] then
					if info.lines[info.line][info.word].space then
						info.dt = info.dt - 1 / self.cps
					end
					info.word = info.word+1
					info.char = 0
				-- Si se ha completado los términos, pasa a la siguiente linea
				else
					local new_height = self.current_height + self.interline + info.lines[info.line+1].height
					local style_dft = self.style.default
					local margins = style_dft.margin[1] + style_dft.margin[3]
					local paddings = style_dft.padding[1] + style_dft.padding[3]
					local max_height = self.height - margins - paddings

					-- Se comprueba si se puede imprimir la linea o, si es está el modo autoskip, levanta las lineas automaticamente
					if (new_height - self.top <= max_height) or (self.autoskip and info.dt > self.delay) then
						info.word = 1
						self.current_height = new_height
						info.line = info.line+1
						info.char = 0
						info.dt = 0

						if self.current_height - self.top > max_height then
							self.line_tween = tween.new(0.5, self, {top = self.top - (info.lines[info.line-1].height + self.interline)}, "inOutQuad")
						end
					-- Si no, se espera a que el usuario accione el método skip
					elseif (new_height - self.top > max_height) and not self.waiting then
						self.waiting = true
					end
				end
			end
		-- Si existe el autoskip, se termina y se saca una nueva linea
		elseif self.autoskip and info.dt > self.delay then
			if info.lines.onfinish then
				info.lines.onfinish()
			end
			pop_line(self)
		-- Si no, se espera a que el usuario accione el método skip
		elseif not self.waiting then
			self.waiting = true
		end
	end
end

--- Draws the dialogue box in the screen.
-- @param x The upper-left x position of the box.
-- @param y The upper-left y position of the box.
function talkative:draw(x, y)
	if self.dialogue then
		local r, g, b, a = love.graphics.getColor()
		local width = love.graphics.getLineWidth()
		love.graphics.setLineWidth(0.5)

		local style_dft = self.style.default
		local max_width = self.width - style_dft.margin[2] - style_dft.margin[4]
		local max_height = self.height - style_dft.margin[1] - style_dft.margin[3]

		-- Se dibuja el fondo que se haya puesto
		if style_dft.background_image then
			love.graphics.draw(style_dft.background_image, x + style_dft.margin[2], y + style_dft.margin[1])
		else
			love.graphics.setColor(style_dft.background)
			love.graphics.rectangle("fill", x + style_dft.margin[2], y + style_dft.margin[1], max_width, max_height)
		end

		local x_pos = x + style_dft.margin[2] + style_dft.padding[2]
		local y_pos = y + style_dft.margin[1] + style_dft.padding[1]
		local rel_width = max_width - style_dft.padding[2] - style_dft.padding[4]
		local rel_height = max_height - style_dft.padding[1] - style_dft.padding[3]

		-- Se warpea el texto si no se está en modo debug, si no se muestra la zona imprimible
		if not self._debug then
			love.graphics.setScissor(x_pos, y_pos, rel_width, rel_height)
		else
			love.graphics.setColor(1, 0.6, 0.25, 0.5)
			love.graphics.rectangle("fill", x, y, self.width, self.height)
			love.graphics.setColor(1, 0.25, 0)
			love.graphics.rectangle("line", x, y, self.width, self.height)
			love.graphics.setColor(0.25, 0.6, 1, 0.5)
			love.graphics.rectangle("fill", x_pos, y_pos, rel_width, rel_height)
			love.graphics.setColor(0, 0.25, 1)
			love.graphics.rectangle("line", x_pos, y_pos, rel_width, rel_height)
		end

		local rel_x = x_pos
		local rel_y = y_pos + self.top

		-- Recorremos todas las líneas de texto
		for i, line in ipairs(self.dialogue.lines) do
			for j, item in ipairs(line) do
				local x_height = line.baseline - item.style.font:getBaseline()

				-- Dibujamos las cajas del texto y la posición actual si estamos en modo debug
				if self._debug then
					love.graphics.setColor(0.25, 1, 0.6, 0.5)
					love.graphics.rectangle("fill", rel_x, rel_y + x_height, item.width, item.style.font:getHeight())
					love.graphics.setColor(0, 1, 0.25)
					love.graphics.rectangle("line", rel_x, rel_y + x_height, item.width, item.style.font:getHeight())

					if self.dialogue.line == i and self.dialogue.word == j then
						local width = item.style.font:getWidth(string.sub(item.text, 1, self.dialogue.char))
						love.graphics.setColor(1, 0.25, 0)
						love.graphics.line(rel_x+width, rel_y, rel_x+width, rel_y + line.height)
					end
				end

				-- Si tiene un estilo diferente al de por defecto, se pinta de fondo el color de texto
				if item.style ~= style_dft then
					local width = item.width

					if self.dialogue.line == i and self.dialogue.word == j then
						width = item.style.font:getWidth(string.sub(item.text, 1, self.dialogue.char))
					elseif item.space then
						width = width + item.style._space
					end

					love.graphics.setColor(item.style.background)
					love.graphics.rectangle("fill", rel_x, rel_y + x_height, width, item.style.font:getHeight())
				end

				-- Se pinta el texto
				love.graphics.setColor(item.style.color)
				love.graphics.draw(item.texture, rel_x, rel_y + x_height)

				rel_x = rel_x + item.width
				if item.space then
					rel_x = rel_x + item.style._space
				end
			end

			-- Si se está en debug, se pinta la linea base
			if self._debug then
				love.graphics.setColor(0, 1, 0.25)
				local x, y = x_pos, rel_y + line.baseline
				love.graphics.line(x, y, x + rel_width, y)
			end

			rel_y = rel_y + line.height + self.interline
			rel_x = x_pos
		end

		love.graphics.setScissor()
		love.graphics.setLineWidth(width)
		love.graphics.setColor(r, g, b, a)
	end
end

return talkative
